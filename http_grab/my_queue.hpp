#include<mutex>
#include<condition_variable>
#include<queue>


/// <summary>
/// 消息队列
/// </summary>
/// <typeparam name="T">消息类型</typeparam>
template<class T> class MessageQueue {
public:
	/// <summary>
	/// 推入消息
	/// </summary>
	/// <param name="msg">消息对象</param>
	void push(const T& msg) {
		std::unique_lock<std::mutex>lck(_mtx);
		_queue.push(msg);
		_cv.notify_one();
	}
	/// <summary>
	/// 轮询消息
	/// </summary>
	/// <param name="msg">消息对象</param>
	/// <returns>是否接收到消息</returns>
	bool poll(T& msg) {
		std::unique_lock<std::mutex>lck(_mtx);
		if (_queue.size())
		{
			msg = _queue.front();
			_queue.pop();
			return true;
		}
		return false;
	}
	/// <summary>
	/// 等待消息
	/// </summary>
	/// <param name="msg">消息对象</param>
	void wait(T& msg) {
		std::unique_lock<std::mutex>lck(_mtx);
		while (!_queue.size()) _cv.wait(lck);
		msg = _queue.front();
		_queue.pop();
	}
	//队列长度
	size_t size() {
		std::unique_lock<std::mutex>lck(_mtx);
		return _queue.size();
	}
private:
	//队列
	std::queue<T> _queue;
	//互斥变量
	std::mutex _mtx;
	//条件变量
	std::condition_variable _cv;
};
