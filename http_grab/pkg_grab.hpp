#ifndef __PKG_GRAB_
#define __PKG_GRAB_

// #include <string>

#define HTTP_PORT 80

#define ETH_ADDR_LENGTH		6

struct ethhdr {
	unsigned char h_dst[ETH_ADDR_LENGTH];
	unsigned char h_src[ETH_ADDR_LENGTH];
	unsigned short h_proto;
	
}; // 14

struct ip_header {
    u_int8_t  	hdrlen : 4,
	    	ip_version : 4;          // 版本和首部长度
    u_int8_t  ip_tos;          // 服务类型
    u_int16_t ip_len;          // 总长度
    u_int16_t ip_id;           // 标识
    u_int16_t ip_off : 13,          // 片偏移
	      flag : 3;
    u_int8_t  ip_ttl;          // 存活时间
    u_int8_t  ip_p;            // 协议类型
    u_int16_t ip_sum;          // 校验和
    struct in_addr ip_src, ip_dst; // 源地址和目的地址
};

struct tcp_header {
    u_int16_t th_sport;        // 源端口号
    u_int16_t th_dport;        // 目的端口号
    u_int32_t th_seq;          // 序列号
    u_int32_t th_ack;          // 确认号
    u_int8_t  th_offx2;        // 数据偏移
    u_int8_t  th_flags;        // 控制标记
    u_int16_t th_win;          // 窗口大小
    u_int16_t th_sum;          // 校验和
    u_int16_t th_urp;          // 紧急指针
};
/*
class PKG_GRAB{
    public:
        PKG_GRAB(char *_net_card) : net_card(_net_card);
        ~PKG_GRAB() {};

        void run(void);

    private:
        string net_card;
    protected:

} ;
*/

#endif
