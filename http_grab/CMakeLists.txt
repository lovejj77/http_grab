cmake_minimum_required(VERSION 3.1)

project (log4cpptest)
# 设置C++标准为 C++ 11
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -W -Wall -Wextra -g")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY /usr/local/sbin)
include_directories(/usr/local/include/)
link_directories(/usr/local/lib)
include_directories(${CMAKE_CURRENT_SOURCE_DIR})

add_executable(http_grab  main.cpp )
target_link_libraries(http_grab log4cpp pthread pcap)
